/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("Mukku", 21, "0621050805"));
        friendList.add(new Friend("Saloth", 20, "0660805105"));
        friendList.add( new Friend("Bogy", 22, "0620810505"));
//        Friend friend1 = new Friend("Mukku", 21, "0621050805");
//        Friend friend2 = new Friend("Saloth", 20, "0660805105");
//        Friend friend3 = new Friend("Bogy", 22, "0620810505");
        FileOutputStream fos = null;
        try {
            // friends.dat
            File file = new File("list_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
//            oos.writeObject(friend1);
//            oos.writeObject(friend2);
//            oos.writeObject(friend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        } //try-catch block - add catch clause
    }
}
